# Vehicle Manage discovery.service project

## Features

This microservice provides the following features:

- Service Discovery
- Load Balancing
- Dynamic Configuration

## Prerequisites

Before getting started, make sure you have the following tools installed:

- Java Development Kit (JDK) 17 or later
- Apache Maven 3.9.1 or later

## Installation

### Clone this repository to your local machine
```bash
git clone https://gitlab.com/adoucour/Vehicle-manager.git
```

### Navigate to the project directory
```bash
cd discovery.service
```

### Build the project using Maven
```bash
mvn clean install
```

### Fill the env.properties file

1. Locate the `env.properties` file in your project at `discovery.service/src/main/resources/env.properties`.

2. Open the `env.properties` file using a text editor.

3. Replace the placeholders with actual values for your environment. For example:

   ```properties
    # Port
   server.port=8761


### Run the application
```bash
mvn spring-boot:run
```

## License

This project is licensed under the MIT License. See the LICENSE file for more information.