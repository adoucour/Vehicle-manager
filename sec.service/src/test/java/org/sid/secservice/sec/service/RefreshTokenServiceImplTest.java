package org.sid.secservice.sec.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import javax.security.auth.RefreshFailedException;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.sid.secservice.sec.JWTUtil;
import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import jakarta.servlet.http.HttpServletResponse;

public class RefreshTokenServiceImplTest {

    @Test
    void testRefreshTokenWithValidHeader() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        AccountService accountServiceMock = Mockito.mock(AccountService.class);

        String username = "admin";
        String roleUser = "ADMIN";

        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(username);

        List<AppRole> appRoles = new ArrayList<>();
        AppRole role = new AppRole();
        role.setRoleName(roleUser);
        appRoles.add(role);

        appUser.setAppRoles(appRoles);

        when(accountServiceMock.loadUserByUsername(anyString())).thenReturn(appUser);

        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET);
        JWTVerifier jwtverifier = JWT.require(algorithm).build();

        long expirationTimeMillis = System.currentTimeMillis() + JWTUtil.getExpireAccessToken();
        Date expirationDate = new Date(expirationTimeMillis);

        String token = JWT.create().withSubject(appUser.getUsername())
                .withExpiresAt(expirationDate)
                .withIssuer("http://localhost:8080/login")
                .withArrayClaim("roles", new String[] { role.getRoleName() })
                .sign(Algorithm.HMAC256(JWTUtil.SECRET));
        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX + token);
        DecodedJWT decodedJWT = jwtverifier.verify(token);

        RefreshTokenServiceImpl refreshTokenServiceMock = new RefreshTokenServiceImpl(accountServiceMock);

        Map<String, String> idToken = refreshTokenServiceMock.refreshToken(request,
                response);

        assertEquals(appUser.getUsername(), decodedJWT.getSubject());
        assertNotEquals(idToken.get("access-token"), decodedJWT.getToken());
        assertNotNull(idToken.get("refresh-token"));
        assertNotNull(idToken.get("access-token"));

        List<String> expectedRoles = appUser.getAppRoles().stream().map(AppRole::getRoleName)
                .collect(Collectors.toList());
        List<String> actualRoles = decodedJWT.getClaim("roles").asList(String.class);

        assertEquals(expectedRoles, actualRoles);
        assertTrue(Math.abs(decodedJWT.getExpiresAt().getTime() - expirationDate.getTime()) <= 1000);

    }

    @Test
    void testRefreshTokenWithInvalidHeader() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(JWTUtil.AUTH_HEADER, "badPrefix" + "invalid_token");

        AccountService accountServiceMock = Mockito.mock(AccountService.class);

        RefreshTokenServiceImpl refreshTokenServiceMock = new RefreshTokenServiceImpl(accountServiceMock);
        MockHttpServletResponse response = new MockHttpServletResponse();

        assertThrows(RefreshFailedException.class, () -> refreshTokenServiceMock.refreshToken(request, response));
    }

    @Test
    void testRefreshTokenWithValidHeaderWithoutToken() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX);

        AccountService accountServiceMock = Mockito.mock(AccountService.class);

        RefreshTokenServiceImpl refreshTokenServiceMock = new RefreshTokenServiceImpl(accountServiceMock);
        MockHttpServletResponse response = new MockHttpServletResponse();

        refreshTokenServiceMock.refreshToken(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());
    }

    @Test
    void testRefreshTokenWithInvalidToken() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX + null);

        AccountService accountServiceMock = Mockito.mock(AccountService.class);

        RefreshTokenServiceImpl refreshTokenServiceMock = new RefreshTokenServiceImpl(accountServiceMock);
        MockHttpServletResponse response = new MockHttpServletResponse();

        refreshTokenServiceMock.refreshToken(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());
    }

    @Test
    void testRefreshTokenWithEmptyHeader() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("", "");

        AccountService accountServiceMock = Mockito.mock(AccountService.class);

        RefreshTokenServiceImpl refreshTokenServiceMock = new RefreshTokenServiceImpl(accountServiceMock);
        MockHttpServletResponse response = new MockHttpServletResponse();

        assertThrows(RefreshFailedException.class, () -> refreshTokenServiceMock.refreshToken(request, response));
    }

}
