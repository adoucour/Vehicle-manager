package org.sid.secservice.sec.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.sid.secservice.sec.exceptions.role.DuplicateRoleException;
import org.sid.secservice.sec.exceptions.role.RoleAlreadyExistsException;
import org.sid.secservice.sec.exceptions.role.RoleNotFoundException;
import org.sid.secservice.sec.exceptions.user.UserAlreadyExistsException;
import org.sid.secservice.sec.exceptions.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ExceptionControllerAdviceTest {

    @Test
    void testHandleDuplicateRoleException() {
        ExceptionControllerAdvice exceptionControllerAdvice = new ExceptionControllerAdvice();
        DuplicateRoleException exception = new DuplicateRoleException("User already has the role.");
        ResponseEntity<String> responseEntity = exceptionControllerAdvice.handleDuplicateRoleException(exception);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("User already has the role.", responseEntity.getBody());
    }

    @Test
    void testHandleRoleAlreadyExistsException() {
        ExceptionControllerAdvice exceptionControllerAdvice = new ExceptionControllerAdvice();
        RoleAlreadyExistsException exception = new RoleAlreadyExistsException("Role already exists.");
        ResponseEntity<String> responseEntity = exceptionControllerAdvice.handleRoleAlreadyExistsException(exception);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Role already exists.", responseEntity.getBody());
    }

    @Test
    void testHandleRoleRoleNotFoundException() {
        ExceptionControllerAdvice exceptionControllerAdvice = new ExceptionControllerAdvice();
        RoleNotFoundException exception = new RoleNotFoundException("Role does not exist.");
        ResponseEntity<String> responseEntity = exceptionControllerAdvice.handleRoleRoleNotFoundException(exception);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Role does not exist.", responseEntity.getBody());
    }

    @Test
    void testHandleUserAlreadyExistsException() {
        ExceptionControllerAdvice exceptionControllerAdvice = new ExceptionControllerAdvice();
        UserAlreadyExistsException exception = new UserAlreadyExistsException("User already exists.");
        ResponseEntity<String> responseEntity = exceptionControllerAdvice.handleUserAlreadyExistsException(exception);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("User already exists.", responseEntity.getBody());
    }

    @Test
    void testHandleUserNotFoundException() {
        ExceptionControllerAdvice exceptionControllerAdvice = new ExceptionControllerAdvice();
        UserNotFoundException exception = new UserNotFoundException("User does not exist.");
        ResponseEntity<String> responseEntity = exceptionControllerAdvice.handleUserNotFoundException(exception);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("User does not exist.", responseEntity.getBody());
    }

}
