package org.sid.secservice.sec.filters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.sid.secservice.sec.JWTUtil;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletResponse;

class JwtAuthorizationFilterTest {

    @Test
    void testDoFilterInternal() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        String jwt = JWT.create().withSubject("admin1")
                .withArrayClaim("roles", new String[] { "ROLE_USER", "ROLE_ADMIN" })
                .sign(Algorithm.HMAC256(JWTUtil.SECRET));
        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX + jwt);
        boolean result = request.getHeader(JWTUtil.AUTH_HEADER) != null
                && request.getHeader(JWTUtil.AUTH_HEADER).startsWith(JWTUtil.PREFIX);

        FilterChain filterChain = mock(FilterChain.class);

        filter.doFilterInternal(request, response, filterChain);

        assertNotNull(SecurityContextHolder.getContext().getAuthentication());
        assertEquals("admin1", SecurityContextHolder.getContext().getAuthentication().getName());
        assertEquals(2, SecurityContextHolder.getContext().getAuthentication().getAuthorities().size());
        assertEquals("ROLE_USER", SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator()
                .next().getAuthority());
        assertEquals(true, result);
    }

    @Test
    void testDoFilterInternalWithInvalidJwt() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        String jwtAuthorizationToken = JWTUtil.PREFIX + "invalid-jwt";
        request.addHeader(JWTUtil.AUTH_HEADER, jwtAuthorizationToken);
        boolean result = request.getHeader(JWTUtil.AUTH_HEADER) != null
                && request.getHeader(JWTUtil.AUTH_HEADER).startsWith(JWTUtil.PREFIX);

        FilterChain filterChain = mock(FilterChain.class);

        try {
            filter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
            assertEquals("Invalid JWT token", response.getContentAsString());
            assertEquals(false, result);
        }
    }

    @Test
    void testDoFilterInternalWithEmptyHeader() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.addHeader("", "");
        FilterChain filterChain = mock(FilterChain.class);

        try {
            filter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
            assertEquals("Invalid JWT token", response.getContentAsString());
        }
    }

    @Test
    void testDoFilterInternalWithHeaderWithoutToken() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX);
        FilterChain filterChain = mock(FilterChain.class);

        try {
            filter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
            assertEquals("Invalid JWT token", response.getContentAsString());
        }
    }

    @Test
    void testDoFilterInternalWithHeaderInvalidToken() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.addHeader(JWTUtil.AUTH_HEADER, JWTUtil.PREFIX + null);
        FilterChain filterChain = mock(FilterChain.class);

        try {
            filter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
            assertEquals("Invalid JWT token", response.getContentAsString());
        }
    }

    @Test
    void testDoFilterInternalWithInvalidHeader() throws Exception {
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.addHeader(JWTUtil.AUTH_HEADER, "BadPrefix" + "token");
        FilterChain filterChain = mock(FilterChain.class);

        try {
            filter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
            assertEquals("Invalid JWT token", response.getContentAsString());
        }
    }

    @Test
    void testDoFilterInternalWithRefreshTokenPath() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        JwtAuthorizationFilter filter = new JwtAuthorizationFilter();

        FilterChain filterChain = mock(FilterChain.class);

        request.setServletPath("/api/refreshToken");

        filter.doFilterInternal(request, response, filterChain);

        Mockito.verify(filterChain).doFilter(request, response);
    }

}