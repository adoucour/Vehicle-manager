package org.sid.secservice.sec.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.sid.secservice.sec.exceptions.role.DuplicateRoleException;
import org.sid.secservice.sec.exceptions.role.RoleAlreadyExistsException;
import org.sid.secservice.sec.exceptions.role.RoleNotFoundException;
import org.sid.secservice.sec.exceptions.user.UserAlreadyExistsException;
import org.sid.secservice.sec.exceptions.user.UserNotFoundException;
import org.sid.secservice.sec.repo.AppRoleRepository;
import org.sid.secservice.sec.repo.AppUserRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

@SpringBootTest
class AccountServiceTest {

    @Mock
    private AppUserRepository appUserRepository;

    @Mock
    private AppRoleRepository appRoleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Captor
    private ArgumentCaptor<AppUser> appUserCaptor;

    @InjectMocks
    private AccountService accountService = new AccountServiceImpl(appUserRepository, appRoleRepository,
            passwordEncoder);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testAddNewUser() {
        AppUser appUser = new AppUser();
        appUser.setUsername("john");
        appUser.setPassword("password");

        String encodedPassword = "encodedPassword";

        when(passwordEncoder.encode(appUser.getPassword())).thenReturn(encodedPassword);
        when(appUserRepository.save(appUser)).thenReturn(appUser);

        AppUser savedUser = accountService.addNewUser(appUser);

        assertEquals(encodedPassword, appUser.getPassword());
        assertEquals(appUser, savedUser);
        verify(passwordEncoder).encode("password");
        verify(appUserRepository, times(1)).save(appUser);
    }

    @Test
    void testAddNewUser_UsernameAlreadyExists() {
        AppUser existingUser = new AppUser();
        existingUser.setUsername("existingUser");
        when(appUserRepository.findByUsername(existingUser.getUsername())).thenReturn(existingUser);

        AppUser newUser = new AppUser();
        newUser.setUsername("existingUser");

        assertThrows(UserAlreadyExistsException.class, () -> accountService.addNewUser(newUser));

        verify(appUserRepository, times(1)).findByUsername(newUser.getUsername());
    }

    @Test
    void testAddNewRole() {
        AppRole appRole = new AppRole();
        appRole.setRoleName("ROLE_ADMIN");

        when(appRoleRepository.save(appRole)).thenReturn(appRole);

        AppRole savedRole = accountService.addNewRole(appRole);

        assertEquals(appRole, savedRole);
        verify(appRoleRepository, times(1)).save(appRole);
    }

    @Test
    void testAddNewRole_RoleAlreadyExists() {
        AppRole existingRole = new AppRole();
        existingRole.setRoleName("existingRole");
        when(appRoleRepository.findByRoleName(existingRole.getRoleName())).thenReturn(existingRole);

        AppRole newRole = new AppRole();
        newRole.setRoleName("existingRole");

        assertThrows(RoleAlreadyExistsException.class, () -> accountService.addNewRole(newRole));

        verify(appRoleRepository, times(1)).findByRoleName(newRole.getRoleName());
    }

    @Test
    void testAddRoleToUser() {
        AppUser user = new AppUser();
        user.setUsername("john");
        user.setPassword("password");

        AppRole role = new AppRole();
        role.setRoleName("ROLE_ADMIN");

        when(appUserRepository.findByUsername(user.getUsername())).thenReturn(user);
        when(appRoleRepository.findByRoleName(role.getRoleName())).thenReturn(role);

        accountService.addRoleToUser(user.getUsername(), role.getRoleName());

        assertEquals(1, user.getAppRoles().size());
        assertEquals(role, user.getAppRoles().iterator().next());
        verify(appUserRepository, times(1)).findByUsername(user.getUsername());
        verify(appRoleRepository, times(1)).findByRoleName(role.getRoleName());
    }

    @Test
    void testAddRoleToUser_UserDoesNotExist() {
        String username = "user1";
        String roleName = "role1";
        when(appUserRepository.findByUsername(username)).thenReturn(null);
        when(appRoleRepository.findByRoleName(roleName)).thenReturn(new AppRole());

        try {
            accountService.addRoleToUser(username, roleName);
            fail("Expected UserNotFoundException to be thrown");
        } catch (UserNotFoundException e) {
            assertEquals("User does not exist.", e.getMessage());
        }
    }

    @Test
    void addRoleToUser_RoleDoesNotExist() {
        String username = "user1";
        String roleName = "role1";

        when(appUserRepository.findByUsername(username)).thenReturn(new AppUser());
        when(appRoleRepository.findByRoleName(roleName)).thenReturn(null);

        try {
            accountService.addRoleToUser(username, roleName);
            fail("Expected RoleNotFoundException to be thrown");
        } catch (RoleNotFoundException e) {
            assertEquals("Role does not exist.", e.getMessage());
        }
    }

    @Test
    void addRoleToUser_UserDoesNotHaveRole() {
        // Arrange
        String username = "user1";
        String roleName = "role1";

        AppUser appUser = new AppUser();
        List<AppRole> appRoles = new ArrayList<>();
        appUser.setAppRoles(appRoles);

        AppRole appRole = new AppRole();
        appRole.setRoleName(roleName);

        when(appUserRepository.findByUsername(username)).thenReturn(appUser);
        when(appRoleRepository.findByRoleName(roleName)).thenReturn(appRole);

        accountService.addRoleToUser(username, roleName);

        assertTrue(appUser.getAppRoles().contains(appRole));
    }

    @Test
    void addRoleToUser_UserAlreadyHasRole() {
        String username = "user1";
        String roleName = "role1";

        AppUser appUser = new AppUser();
        List<AppRole> appRoles = new ArrayList<>();
        AppRole existingRole = new AppRole();
        existingRole.setRoleName(roleName);
        appRoles.add(existingRole);
        appUser.setAppRoles(appRoles);

        when(appUserRepository.findByUsername(username)).thenReturn(appUser);
        when(appRoleRepository.findByRoleName(roleName)).thenReturn(new AppRole());

        assertThrows(DuplicateRoleException.class, () -> accountService.addRoleToUser(username, roleName));
    }

    @Test
    void testLoadUserByUsername() {
        AppUser user = new AppUser();
        user.setId(1L);
        user.setUsername("john");
        user.setPassword("password");

        when(appUserRepository.findByUsername(user.getUsername())).thenReturn(user);

        AppUser userDetails = accountService.loadUserByUsername(user.getUsername());

        assertEquals(user.getUsername(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
        assertEquals(user.getId(), userDetails.getId());
    }

    @Test
    void testListUsers() {
        AppUser user1 = new AppUser();
        user1.setId(1L);
        user1.setUsername("john");
        user1.setPassword("password");

        AppUser user2 = new AppUser();
        user2.setId(2L);
        user2.setUsername("mary");
        user2.setPassword("password123");

        List<AppUser> expectedUsers = Arrays.asList(user1, user2);
        when(appUserRepository.findAll()).thenReturn(expectedUsers);

        List<AppUser> actualUsers = accountService.listUsers();

        assertEquals(expectedUsers.size(), actualUsers.size());
        for (int i = 0; i < expectedUsers.size(); i++) {
            AppUser expectedUser = expectedUsers.get(i);
            AppUser actualUser = actualUsers.get(i);
            assertEquals(expectedUser.getId(), actualUser.getId());
            assertEquals(expectedUser.getUsername(), actualUser.getUsername());
            assertEquals(expectedUser.getPassword(), actualUser.getPassword());
        }

    }

}
