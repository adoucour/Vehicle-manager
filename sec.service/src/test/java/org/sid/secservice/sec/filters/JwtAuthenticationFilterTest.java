package org.sid.secservice.sec.filters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;

import java.io.IOException;
import java.util.Collections;

public class JwtAuthenticationFilterTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private Authentication authResult;

    @InjectMocks
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @BeforeEach
    void setUp() {
        authenticationManager = mock(AuthenticationManager.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
        authResult = mock(Authentication.class);

        jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager);
    }

    @Test
    void attemptAuthentication_ReturnsAuthentication() throws ServletException, IOException {
        when(request.getParameter("username")).thenReturn("john");
        when(request.getParameter("password")).thenReturn("pass123");

        Authentication authentication = mock(Authentication.class);
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(authentication);


        jwtAuthenticationFilter.attemptAuthentication(request, response);

        verify(request, times(1)).getParameter("username");
        verify(request, times(1)).getParameter("password");
        verify(authenticationManager, times(1)).authenticate(any(UsernamePasswordAuthenticationToken.class));
    }

    @Test
    void successfulAuthentication() throws ServletException, IOException {
        User principal = new User("username", "password",
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));
        when(authResult.getPrincipal()).thenReturn(principal);

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost:8080/"));

        ServletOutputStream outputStream = mock(ServletOutputStream.class);
        when(response.getOutputStream()).thenReturn(outputStream);

        jwtAuthenticationFilter.successfulAuthentication(request, response, filterChain, authResult);

        verify(response).setContentType("application/json");
    }
}
