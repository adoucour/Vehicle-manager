package org.sid.secservice.service;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.sid.secservice.sec.service.AccountService;
import org.sid.secservice.sec.service.UserDetailsServiceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserDetailsServiceImplTest {

    @Test
    public void testLoadUserByUsername() {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        UserDetailsServiceImpl userDetailsServiceMock = new UserDetailsServiceImpl(accountServiceMock);

        String username = "testUser";
        String password = "testPassword";

        List<AppRole> appRoles = new ArrayList<>();
        AppRole role = new AppRole();
        role.setRoleName("ROLE_ADMIN");
        appRoles.add(role);

        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(password);
        appUser.setAppRoles(appRoles);

        when(accountServiceMock.loadUserByUsername(username)).thenReturn(appUser);

        UserDetails userDetails = userDetailsServiceMock.loadUserByUsername(username);

        assertNotNull(userDetails);
        assertEquals(username, userDetails.getUsername());
        assertEquals(password, userDetails.getPassword());

        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        assertNotNull(authorities);
        assertEquals(1, authorities.size());
        assertTrue(authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN")));

        verify(accountServiceMock, times(1)).loadUserByUsername(username);
    }
}
