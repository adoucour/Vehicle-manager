package org.sid.secservice.web;

import java.util.List;
import java.util.Map;

import javax.security.auth.RefreshFailedException;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.sid.secservice.sec.entities.RoleUserForm;
import org.sid.secservice.sec.service.AccountService;
import org.sid.secservice.sec.service.RefreshTokenService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AccountRestControllerTest {

    @Test
    void testAppUsers() {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);

        List<AppUser> users = new ArrayList<>();

        AppUser user1 = new AppUser();
        user1.setId(1L);
        user1.setUsername("john");
        user1.setPassword("password");

        AppUser user2 = new AppUser();
        user2.setId(2L);
        user2.setUsername("maria");
        user2.setPassword("password");

        AppUser user3 = new AppUser();
        user3.setId(3L);
        user3.setUsername("nathan");
        user3.setPassword("password");

        users.add(user1);
        users.add(user2);
        users.add(user3);

        Mockito.when(accountServiceMock.listUsers()).thenReturn(users);

        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);

        List<AppUser> result = accountRestController.appUsers();

        assertEquals(users, result);
    }

    @Test
    void testSaveUser() {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);

        AppUser user = new AppUser();
        user.setId(1L);
        user.setUsername("toto");
        user.setPassword("password");

        when(accountServiceMock.addNewUser(any(AppUser.class))).thenReturn(user);

        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);

        AppUser result = accountRestController.saveUser(user);

        assertEquals(user, result);
        verify(accountServiceMock).addNewUser(user);
    }

    @Test
    void testSaveRole() {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);

        AppRole role = new AppRole();
        role.setId(1L);
        role.setRoleName("ROLE_ADMIN");

        when(accountServiceMock.addNewRole(any(AppRole.class))).thenReturn(role);

        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);

        AppRole result = accountRestController.saveRole(role);

        assertEquals(role, result);
        verify(accountServiceMock).addNewRole(role);
    }

    @Test
    void testAddRoleToUser() {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);

        RoleUserForm roleUserForm = new RoleUserForm();
        roleUserForm.setUsername("admin1");
        roleUserForm.setRoleName("ROLE_ADMIN");

        doNothing().when(accountServiceMock).addRoleToUser(roleUserForm.getUsername(), roleUserForm.getRoleName());

        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);
        accountRestController.addRoleToUser(roleUserForm);

        verify(accountServiceMock).addRoleToUser(roleUserForm.getUsername(), roleUserForm.getRoleName());
    }

    @Test
    void testRefreshToken() throws RefreshFailedException, IOException {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        Map<String, String> refreshTokenResult = new HashMap<>();
        refreshTokenResult.put("access_token", "new_access_token");
        refreshTokenResult.put("refresh_token", "new_refresh_token");

        when(refreshTokenService.refreshToken(request, response)).thenReturn(refreshTokenResult);

        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);

        Map<String, String> result = accountRestController.refreshToken(request, response);

        verify(refreshTokenService, times(1)).refreshToken(request, response);
        assertEquals("new_access_token", result.get("access_token"));
        assertEquals("new_refresh_token", result.get("refresh_token"));
    }

    @Test
    void testGetProfile() throws Exception {
        AccountService accountServiceMock = Mockito.mock(AccountService.class);
        RefreshTokenService refreshTokenService = Mockito.mock(RefreshTokenService.class);
        Principal principal = mock(Principal.class);
        AccountRestController accountRestController = new AccountRestController(accountServiceMock,
                refreshTokenService);

        when(principal.getName()).thenReturn("testuser");

        AppUser mockUser = new AppUser();
        when(accountServiceMock.loadUserByUsername("testuser")).thenReturn(mockUser);
        AppUser result = accountRestController.getProfile(principal);

        verify(accountServiceMock).loadUserByUsername("testuser");
        assertEquals(mockUser, result);

    }

}
