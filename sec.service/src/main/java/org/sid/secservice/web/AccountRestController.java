package org.sid.secservice.web;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.security.auth.RefreshFailedException;

import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.sid.secservice.sec.entities.RoleUserForm;
import org.sid.secservice.sec.service.AccountService;
import org.sid.secservice.sec.service.RefreshTokenService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class AccountRestController {
    private AccountService accountService;
    private RefreshTokenService refreshTokenService;

    public AccountRestController(AccountService accountService, RefreshTokenService refreshTokenService) {
        this.accountService = accountService;
        this.refreshTokenService = refreshTokenService;
    }

    @Operation(summary = "Get all users")
    @GetMapping(path = "account/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<AppUser> appUsers() {
        return accountService.listUsers();
    }

    @Operation(summary = "Save a new user")
    @PostMapping(path = "account/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public AppUser saveUser(
            @RequestBody @Schema(description = "The user details", format = "json", example = "{\"username\":\"string\",\"password\":\"string\"}") AppUser appUser) {
        return accountService.addNewUser(appUser);
    }

    @Operation(summary = "Save a new role")
    @PostMapping(path = "account/roles")
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public AppRole saveRole(
            @RequestBody @Schema(description = "The role details", format = "json", example = "{\"roleName\":\"string\"}") AppRole appRole) {
        return accountService.addNewRole(appRole);
    }

    @Operation(summary = "Add a role to a user")
    @PostMapping(path = "account/addRoleToUser")
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public void addRoleToUser(@RequestBody RoleUserForm roleUserForm) {
        accountService.addRoleToUser(roleUserForm.getUsername(), roleUserForm.getRoleName());
    }

    @Operation(summary = "Refresh user's token")
    @GetMapping(path = "refreshToken")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws RefreshFailedException, IOException {
        return refreshTokenService.refreshToken(request, response);
    }

    @Operation(summary = "Allows you to find out who is logged in")
    @GetMapping(path = "account/profile")
    public AppUser getProfile(Principal principal) {
        return accountService.loadUserByUsername(principal.getName());
    }

}
