package org.sid.secservice.sec.service;

import java.util.List;

import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.sid.secservice.sec.exceptions.role.DuplicateRoleException;
import org.sid.secservice.sec.exceptions.role.RoleAlreadyExistsException;
import org.sid.secservice.sec.exceptions.role.RoleNotFoundException;
import org.sid.secservice.sec.exceptions.user.UserAlreadyExistsException;
import org.sid.secservice.sec.exceptions.user.UserNotFoundException;
import org.sid.secservice.sec.repo.AppUserRepository;
import org.sid.secservice.sec.repo.AppRoleRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    private AppUserRepository appUserRepository;
    private AppRoleRepository appRoleRepository;

    private PasswordEncoder passwordEncoder;

    public AccountServiceImpl(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository,
            PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUser addNewUser(AppUser appUser) {
        AppUser existingUser = appUserRepository.findByUsername(appUser.getUsername());
        if (existingUser != null) {
            throw new UserAlreadyExistsException("User already exists.");
        }

        String pwd = appUser.getPassword();
        appUser.setPassword(passwordEncoder.encode(pwd));
        return appUserRepository.save(appUser);
    }

    @Override
    public AppRole addNewRole(AppRole approle) {
        AppRole existingRole = appRoleRepository.findByRoleName(approle.getRoleName());
        if (existingRole != null) {
            throw new RoleAlreadyExistsException("Role already exists.");
        }
        return appRoleRepository.save(approle);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppUser appUser = appUserRepository.findByUsername(username);
        AppRole appRole = appRoleRepository.findByRoleName(roleName);

        if (appUser == null) {
            throw new UserNotFoundException("User does not exist.");
        }

        if (appRole == null) {
            throw new RoleNotFoundException("Role does not exist.");
        }

        if (appUser.getAppRoles().stream().anyMatch(role -> role.getRoleName().equals(roleName))) {
            throw new DuplicateRoleException("User already has the role.");
        }

        appUser.getAppRoles().add(appRole);
    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public List<AppUser> listUsers() {
        return appUserRepository.findAll();
    }

}
