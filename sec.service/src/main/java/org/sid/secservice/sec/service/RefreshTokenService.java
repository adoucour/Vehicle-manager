package org.sid.secservice.sec.service;

import java.io.IOException;
import java.util.Map;

import javax.security.auth.RefreshFailedException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface RefreshTokenService {
    Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws RefreshFailedException, IOException;
}
