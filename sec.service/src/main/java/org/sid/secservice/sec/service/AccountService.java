package org.sid.secservice.sec.service;

import java.util.List;

import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;

public interface AccountService {
    AppUser addNewUser(AppUser appuser);
    AppRole addNewRole(AppRole approle);
    void addRoleToUser(String username, String rolename);
    AppUser loadUserByUsername(String username);
    List<AppUser> listUsers();
}
