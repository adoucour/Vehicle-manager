package org.sid.secservice.sec;

public class JWTUtil {
    public static final String SECRET = System.getenv("MY_SECRET");
    public static final String AUTH_HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";

    public static final long getExpireAccessToken() {
        String expireAccessToken = System.getenv("EXPIRE_ACCESS_TOKEN");
        return Long.parseLong(expireAccessToken);
    }

    public static final long getExpireRefreshToken() {
        String expireRefreshToken = System.getenv("EXPIRE_REFRESH_TOKEN");
        return Long.parseLong(expireRefreshToken);
    }

}
