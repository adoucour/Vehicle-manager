package org.sid.secservice.sec.exceptions;

import org.sid.secservice.sec.exceptions.role.DuplicateRoleException;
import org.sid.secservice.sec.exceptions.role.RoleAlreadyExistsException;
import org.sid.secservice.sec.exceptions.role.RoleNotFoundException;
import org.sid.secservice.sec.exceptions.user.UserAlreadyExistsException;
import org.sid.secservice.sec.exceptions.user.UserNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import jakarta.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ExceptionControllerAdvice {

    // roles
    @ExceptionHandler(DuplicateRoleException.class)
    public ResponseEntity<String> handleDuplicateRoleException(DuplicateRoleException exception) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(RoleAlreadyExistsException.class)
    public ResponseEntity<String> handleRoleAlreadyExistsException(RoleAlreadyExistsException exception) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(RoleNotFoundException.class)
    public ResponseEntity<String> handleRoleRoleNotFoundException(RoleNotFoundException exception) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).body(exception.getMessage());
    }

    // user
    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<String> handleUserAlreadyExistsException(UserAlreadyExistsException exception) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).body(exception.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException exception) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).body(exception.getMessage());
    }

}
