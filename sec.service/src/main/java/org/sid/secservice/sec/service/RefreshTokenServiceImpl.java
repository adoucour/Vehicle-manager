package org.sid.secservice.sec.service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.security.auth.RefreshFailedException;

import org.sid.secservice.sec.JWTUtil;
import org.sid.secservice.sec.entities.AppRole;
import org.sid.secservice.sec.entities.AppUser;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
    private AccountService accountService;

    public RefreshTokenServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws RefreshFailedException, IOException {
        String jwtAuthorizationToken = request.getHeader(JWTUtil.AUTH_HEADER);
        if (jwtAuthorizationToken != null && jwtAuthorizationToken.startsWith(JWTUtil.PREFIX)) {
            try {
                String jwt = jwtAuthorizationToken.substring(JWTUtil.PREFIX.length());
                Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET);
                JWTVerifier jwtverifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtverifier.verify(jwt);
                String username = decodedJWT.getSubject();

                AppUser appUser = accountService.loadUserByUsername(username);
                String jwtAcessToken = JWT.create()
                        .withSubject(appUser.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.getExpireAccessToken()))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles",
                                appUser.getAppRoles().stream().map(AppRole::getRoleName).collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> idToken = new HashMap<>();
                idToken.put("access-token", jwtAcessToken);
                idToken.put("refresh-token", jwt);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(), idToken);
                return idToken;

            } catch (Exception e) {
                response.setHeader("error-message", e.getMessage());
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }

        }

        else {
            throw new RefreshFailedException("Refresh token required !");
        }
        return new HashMap<>();
    }

}
