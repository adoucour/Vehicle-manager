package org.sid.secservice.sec.filters;

import java.io.IOException;

import org.sid.secservice.sec.JWTUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
        private AuthenticationManager authenticationManager;

        public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
                this.authenticationManager = authenticationManager;
        }

        @Override
        public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
                        throws AuthenticationException {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                                username,
                                password);

                return authenticationManager.authenticate(authenticationToken);
        }

        @Override
        protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                        FilterChain chain,
                        Authentication authResult) throws IOException, ServletException {
                User user = (User) authResult.getPrincipal();
                Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET);

                List<String> roles = user.getAuthorities().stream()
                                .map(ga -> ga.getAuthority())
                                .collect(Collectors.toList());

                String jwtAcessToken = JWT.create()
                                .withSubject(user.getUsername())
                                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.getExpireAccessToken()))
                                .withIssuer(request.getRequestURL().toString())
                                .withClaim("roles", roles)
                                .sign(algorithm);

                String jwtRefreshToken = JWT.create()
                                .withSubject(user.getUsername())
                                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.getExpireRefreshToken()))
                                .withIssuer(request.getRequestURL().toString())
                                .sign(algorithm);

                Map<String, Object> idToken = new HashMap<>();
                idToken.put("access_token", jwtAcessToken);
                idToken.put("refresh_token", jwtRefreshToken);
                idToken.put("username", user.getUsername());
                idToken.put("roles", roles);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(), idToken);
        }

}
