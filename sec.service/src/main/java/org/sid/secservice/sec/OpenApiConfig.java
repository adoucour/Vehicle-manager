package org.sid.secservice.sec;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI usersMicroserviceOpenAPI() {
        return new OpenAPI()
        .info(new Info().title("Vehicle manager API")
            .description("The Vehicle Manager API provides endpoints to manage authentication and vehicles, including creating, updating, retrieving, and deleting vehicle records. It allows users to perform operations such as adding new vehicles, updating vehicle information, retrieving vehicle details, and deleting vehicle records from the system.")
            .version("1.0"))
        .addServersItem(new Server().url("https://localhost:8080/vehicle-manager"))
        .components(new Components()
            .addSecuritySchemes("api-key",
                new SecurityScheme()
                    .type(SecurityScheme.Type.APIKEY)
                    .in(SecurityScheme.In.HEADER)
                    .name("Authorization")
            )
        );
    }

}
