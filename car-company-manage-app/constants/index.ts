export const app_url = {
  login: '/login',
  home: '/',
};

export const service_name = {
  security: '/SEC-SERVICE',
};

export const app_message = {
  errOccurred: 'An error occurred, please contact your admin',
};
