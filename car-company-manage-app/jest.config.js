module.exports = {
    preset: "ts-jest",
    testEnvironment: "jest-environment-jsdom",
    transform: {
        "^.+\\.(js|jsx|ts|tsx)$": "babel-jest"
    },
    collectCoverage: true,
    coverageProvider: 'v8',
    collectCoverageFrom: [
        '**/*.{js,jsx,ts,tsx}',
        '!**/*.d.ts',
        '!**/node_modules/**',
        '!<rootDir>/out/**',
        '!<rootDir>/.next/**',
        '!<rootDir>/*.config.js',
        '!<rootDir>/.prettierrc.js',
        '!<rootDir>/app/layout.tsx',
        '!<rootDir>/components/Providers.tsx',
        '!<rootDir>/app/api/auth/**',
        '!<rootDir>/types/**',
        '!<rootDir>/coverage/**',
    ],
    //setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
        "\\.(css|less)$": "identity-obj-proxy",
        '^@/components': '<rootDir>/components/',
        '^@/constants': '<rootDir>/constants/',
        '^@/utils': '<rootDir>/utils/',
        '^@/services': '<rootDir>/services/',
      },
      testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/.next/'],
      transformIgnorePatterns: [
        '/node_modules/',
        '^.+\\.module\\.(css|sass|scss)$',
      ],
};