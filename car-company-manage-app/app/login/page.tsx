'use client';

import { CustomButton } from '@/components';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { signIn, useSession } from 'next-auth/react';
import { manage_auth } from '@/utils';
import { app_message, app_url } from '@/constants';

const Page = () => {
  const [user, setUser] = useState({
    username: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const router = useRouter();
  const { data: session } = useSession();

  const onLogin = async () => {
    try {
      setLoading(true);
      const response = await signIn('credentials', {
        username: user.username,
        password: user.password,
        redirect: false,
      });

      if (response?.error && session == null) {
        const err: string = manage_auth.signInError(response.error);
        toast.error(err);
        return;
      }

      toast.success('Login success');
      redirectToHme();
    } catch (error: unknown) {
      if (error instanceof Error) {
        toast.error(error.message);
      } else {
        toast.error(app_message.errOccurred);
      }
    } finally {
      setLoading(false);
    }
  };

  const redirectToHme = () => {
    router.push(app_url.home);
  };

  useEffect(() => {
    user.username.length > 3 && user.password.length > 3
      ? setButtonDisabled(false)
      : setButtonDisabled(true);
  }, [user]);

  return (
    <div className="flex items-center justify-center min-h-screen bg-gray-100">
      <div className="relative flex flex-col m-6 space-y-8 bg-white shadow-2xl rounded-2xl md:flex-row md:space-y-0">
        <div className="flex flex-col justify-center p-8 md:p-14">
          <span className="mb-3 text-4xl font-bold">Welcome !</span>
          <span className="font-light text-gray-400 mb-8">
            {loading ? 'Processing' : 'Please enter your details'}
          </span>
          <div className="py-4">
            <span className="mb-2 text-md">Username</span>
            <input
              type="text"
              value={user.username}
              onChange={(e) => setUser({ ...user, username: e.target.value })}
              className="w-full p-2 border border-gray-300 rounded-md placeholder:font-light placeholder:text-gray-500"
              name="username"
              id="username"
              data-testid="username"
            />
          </div>
          <div className="py-4">
            <span className="mb-2 text-md">Password</span>
            <input
              type="password"
              value={user.password}
              onChange={(e) => setUser({ ...user, password: e.target.value })}
              className="w-full p-2 border border-gray-300 rounded-md placeholder:font-light placeholder:text-gray-500"
              name="password"
              id="password"
              data-testid="password"
            />
          </div>
          <CustomButton
            title="Sign in"
            btnType="button"
            containerStyles="w-full bg-primary-blue text-white p-2 rounded-lg mb-6"
            handleClick={onLogin}
            disabled={buttonDisabled}
          />
          <CustomButton
            title="Cancel"
            btnType="button"
            containerStyles="w-full bg-primary-red text-white p-2 rounded-lg mb-6"
            handleClick={redirectToHme}
          />
        </div>

        <Toaster />

        <div className="relative">
          <Image
            src="/login.jpg"
            alt="login"
            width={400}
            height={0}
            className="h-full hidden rounded-r-2xl md:block object-cover"
            style={{ width: 'auto' }}
          />
        </div>
      </div>
    </div>
  );
};

export default Page;
