import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { signIn } from 'next-auth/react';
import '@testing-library/jest-dom';
import Page from './page';

jest.mock('next-auth/react', () => {
  const originalModule = jest.requireActual('next-auth/react');
  const mockSession = null;
  return {
    __esModule: true,
    ...originalModule,
    useSession: jest.fn(() => {
      return { data: mockSession, status: 'unauthenticated' };
    }),
    signIn: jest.fn(),
    signOut: jest.fn(),
  };
});

jest.mock('next/navigation', () => ({
  useRouter: () => ({
    push: jest.fn(),
  }),
}));

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

describe('Login page component', () => {
  it('renders inputs and buttons correctly', () => {
    render(<Page />);

    const usernameInput = screen.getByText('Username');
    const passwordInput = screen.getByText('Password');
    const signInButton = screen.getByRole('button', { name: 'Sign in' });
    const cancelButton = screen.getByRole('button', { name: 'Cancel' });

    expect(usernameInput).toBeInTheDocument();
    expect(passwordInput).toBeInTheDocument();
    expect(signInButton).toBeInTheDocument();
    expect(cancelButton).toBeInTheDocument();
  });

  it('disables Sign in button initially', () => {
    render(<Page />);
    const signInButton = screen.getByRole('button', { name: 'Sign in' });
    expect(signInButton).toBeDisabled();
  });

  it('enables Sign in button when username and password are both longer than 3 characters', () => {
    render(<Page />);

    const usernameInput = screen.getByTestId('username');
    const passwordInput = screen.getByTestId('password');
    const signInButton = screen.getByRole('button', { name: 'Sign in' });

    fireEvent.change(usernameInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'testpass' } });

    expect(signInButton).not.toBeDisabled();
  });

  it('calls signIn function and redirects on successful login', async () => {
    render(<Page />);

    const usernameInput = screen.getByTestId('username');
    const passwordInput = screen.getByTestId('password');
    const signInButton = screen.getByRole('button', { name: 'Sign in' });

    fireEvent.change(usernameInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'testpass' } });
    fireEvent.click(signInButton);

    await waitFor(() => {
      expect(signIn).toHaveBeenCalledWith('credentials', {
        password: 'testpass',
        redirect: false,
        username: 'testuser',
      });
    });
  });

  it('should show an error message if the signIn response has an error', async () => {
    (signIn as jest.Mock).mockReturnValueOnce({
      error: 'An error occurred',
      session: null,
    });

    render(<Page />);

    const usernameInput = screen.getByTestId('username');
    const passwordInput = screen.getByTestId('password');
    const signInButton = screen.getByRole('button', { name: 'Sign in' });

    fireEvent.change(usernameInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'testpass' } });
    fireEvent.click(signInButton);

    await waitFor(() => {
      expect(signIn).toBeCalledWith('credentials', {
        password: 'testpass',
        redirect: false,
        username: 'testuser',
      });
    });
  });
});
