import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import Home from './page';

jest.mock('@/components/Hero', () => {
  return {
    __esModule: true,
    Hero: () => <div data-testid="mock-hero">Mocked Hero Component</div>,
  };
});

describe('Home Page', () => {
  it('renders the Home page correctly', () => {
    const { getByText, getByTestId } = render(<Home />);

    const heroComponent = getByTestId('mock-hero');
    expect(heroComponent).toBeInTheDocument();

    const welcomeText = getByText('Welcome:');
    expect(welcomeText).toBeInTheDocument();
  });
});
