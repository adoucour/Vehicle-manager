import NextAuth from 'next-auth/next';
import CredentialsProvider from 'next-auth/providers/credentials';
import { service } from '@/services';
import { NextAuthOptions } from 'next-auth';
import { app_url } from '@/constants';
import { manage_auth } from '@/utils';

const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      type: 'credentials',
      credentials: {
        username: { label: 'username', type: 'text' },
        password: { label: 'password', type: 'password' },
      },

      async authorize(credentials) {
        const { username, password } = credentials as {
          username: string;
          password: string;
        };

        const signIn = await service.login(username, password);
        const user = await signIn.json();
        if (manage_auth.isSignInResponse(user)) {
          return user;
        } else return null;
      },
    }),
  ],
  pages: {
    signIn: app_url.login,
  },
  callbacks: {
    async jwt({ token, user, session }) {
      console.log('jwt callbacks: ', { token, user, session });
      return { ...token, ...user };
    },

    async session({ session, token, user }) {
      session.user = token;
      console.log('session callbacks :', { token, user, session });
      return session;
    },
  },
  session: {
    strategy: 'jwt',
  },
  secret: process.env.NEXTAUTH_SECRET,
  debug: process.env.NODE_ENV === 'development',
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
