module.exports = {
	printWidth: 80,
	semi: true,
	singleQuote: true,
	tabWidth: 2,
	useTabs: false,
	bracketSpacing: true,
	bracketSameLine: false,
	arrowParens: "always",
	endOfLine: "auto",
	quoteProps: "as-needed"
  };