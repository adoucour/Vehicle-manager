import { MouseEventHandler } from 'react';

export interface CustomButtonProps {
  title: string;
  containerStyles?: string;
  handleClick?: MouseEventHandler<HTMLButtonElement>;
  btnType?: 'button' | 'submit';
  disabled?: boolean;
}

//next-auth.d.ts
declare module 'next-auth' {
  export interface User {
    roles: string[];
    username: string;
    access_token: string;
    refresh_token: string;
  }
  interface Session {
    user: User;
  }
}

//next-auth.d.ts
declare module 'next-auth/jwt' {
  export interface JWT {
    id: string;
    roles: string[];
    username: string;
    access_token: string;
    refresh_token: string;
  }
}
