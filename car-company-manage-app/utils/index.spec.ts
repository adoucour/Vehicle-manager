import { manage_auth } from '.';
import { User } from 'next-auth';

describe('manage_auth', () => {
  describe('signInErrorTest', () => {
    it('returns the correct error message for "fetch failed"', () => {
      const errorMessage = manage_auth.signInError('fetch failed');
      expect(errorMessage).toBe(
        'Security server not available, please contact your admin',
      );
    });

    it('returns the correct error message for other cases', () => {
      const errorMessage = manage_auth.signInError('other error');
      expect(errorMessage).toBe('Login failed: Invalid username or password');
    });
  });

  describe('isSignInResponse', () => {
    it('returns true for a valid sign-in response', () => {
      const validUser: User = {
        id: '1',
        access_token: 'token',
        refresh_token: 'refresh_token',
        roles: ['user'],
        username: 'example',
      };
      const result = manage_auth.isSignInResponse(validUser);
      expect(result).toBe(true);
    });

    it('returns false if any property is missing', () => {
      const incompleteUser: User = {
        id: '2',
        access_token: 'token',
        refresh_token: 'refresh_token',
        username: '',
        roles: ['user'],
      };
      const result = manage_auth.isSignInResponse(incompleteUser);
      expect(result).toBe(false);
    });
  });
});
