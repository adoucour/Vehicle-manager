import { User } from 'next-auth';

export const manage_auth = {
  signInError: (value: string): string => {
    return value === 'fetch failed'
      ? 'Security server not available, please contact your admin'
      : 'Login failed: Invalid username or password';
  },
  isSignInResponse: (obj: User): boolean => {
    return !!(
      obj?.access_token &&
      obj?.refresh_token &&
      obj?.roles &&
      obj?.username
    );
  },
};
