This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# car-front

This repository contain the car-company-manage-app app

## Requirements

To run this application on your workstation, you must install :

-   node.js version 18.17.xx
-   npm 9.8.x
-   React 18.2
-   Next.js 13.4.XX

### Clone this repository
```bash
git clone https://gitlab.com/adoucour/Vehicle-manager.git
```

### Navigate to the project directory
```bash
cd car-company-manage-app
```

### Install dependencies
To generate node_modules folder who contains dependencies.
```bash
npm install
```

### Run development mode
```bash
npm run dev
```

Navigate to `http://localhost:3000/`. The application will automatically reload if you change any of the source files.

### Run development mode and debug Next server
```bash
npm run dev:debug-srv-local
```

#### For Linux or MacOS users, replace
```json
"cross-env NODE_OPTIONS='--inspect' next dev",
```
By 
```json
"NODE_OPTIONS='--inspect' next dev",
```

#### VsCode config debug
```json
{
    "version": "0.2.0",
    "configurations": [
      {
        "type": "node",
        "request": "attach",
        "name": "Launch Program",
        "skipFiles": [
          "<node_internals>/**"
        ],
        "port": 9229
      }
    ]
  }
```

### Running [eslint](https://eslint.org/)
Check js rules
```bash
npm run lint
```

### Running [prettier ](https://prettier.io/)
Format the code
```bash
npm run format
```

Check your format code
```bash
npm run format:check
```

## Build
To build this project, build files will be at the root of the project in the dist directory.
```bash
npm run build
```

### Run production server (require build files)
```bash
npm run start
```