import { service } from '.';

describe('Service', () => {
  describe('login', () => {
    it('should make a POST request with the correct data', async () => {
      const mockFetch = jest.fn();
      (global as any).fetch = mockFetch;

      const mockResponse = {
        ok: true,
        json: jest.fn().mockResolvedValue({}),
      };
      mockFetch.mockResolvedValue(mockResponse);

      process.env.NEXT_PUBLIC_API_URL = 'https://example.com/api';

      const username: string = 'testUser';
      const password: string = 'testPassword';
      await service.login(username, password);

      expect(mockFetch).toHaveBeenCalledWith(
        'https://example.com/api/login',
        expect.objectContaining({
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'username=' + username + '&password=' + password,
        }),
      );

      process.env.NEXT_PUBLIC_API_URL = undefined;
    });
  });
});
