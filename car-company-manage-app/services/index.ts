import { app_url, service_name } from '@/constants';

export const service = {
  login: async (username: string, password: string) => {
    const url =
      (process.env.NEXT_PUBLIC_API_URL as string) +
      service_name.security +
      app_url.login;
    const formData = new URLSearchParams();
    formData.append('username', username);
    formData.append('password', password);

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formData.toString(),
    });
    const data = Promise.resolve(response);
    return data;
  },
};
