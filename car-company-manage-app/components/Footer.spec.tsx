import React from 'react';
import { render, screen } from '@testing-library/react';
import { usePathname } from 'next/navigation';
import Footer from './Footer';
import '@testing-library/jest-dom';
import { app_url } from '@/constants';

jest.mock('next/navigation', () => {
  const originalModule = jest.requireActual('next/navigation');
  return {
    __esModule: true,
    ...originalModule,
    usePathname: jest.fn(),
  };
});

describe('Footer component', () => {
  it('displays the footer when pathname is not in the hidden list', () => {
    (usePathname as jest.Mock).mockReturnValue('/some-page');
    render(<Footer />);

    expect(
      screen.getByText((content) => content.includes('Carhub 2023')),
    ).toBeInTheDocument();
    expect(
      screen.getByText((content) =>
        content.includes('@2023 CarHub. All rights reserved'),
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText((content) => content.includes('Privacy & Policy')),
    ).toBeInTheDocument();
    expect(
      screen.getByText((content) => content.includes('Terms & Condition')),
    ).toBeInTheDocument();
  });

  it('does not display the footer when pathname is in the hidden list', () => {
    (usePathname as jest.Mock).mockReturnValue(app_url.login);

    render(<Footer />);

    expect(
      screen.queryByText((content) => content.includes('Carhub 2023')),
    ).toBeNull();
    expect(
      screen.queryByText((content) =>
        content.includes('@2023 CarHub. All rights reserved'),
      ),
    ).toBeNull();
    expect(
      screen.queryByText((content) => content.includes('Privacy & Policy')),
    ).toBeNull();
    expect(
      screen.queryByText((content) => content.includes('Terms & Condition')),
    ).toBeNull();
  });
});
