import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import NavBar from './Navbar';
import '@testing-library/jest-dom';
import { signIn, useSession, signOut } from 'next-auth/react';

jest.mock('next-auth/react', () => {
  const originalModule = jest.requireActual('next-auth/react');
  const mockSession = null;
  return {
    __esModule: true,
    ...originalModule,
    useSession: jest.fn(() => {
      return { data: mockSession, status: 'unauthenticated' };
    }),
    signIn: jest.fn(),
    signOut: jest.fn(),
  };
});

describe('Navbar component', () => {
  it('renders logo and sign-in button when user is not authenticated', async () => {
    render(<NavBar />);

    const logo = screen.getByAltText('logo');
    expect(logo).toBeInTheDocument();

    const signInButton = screen.getByText('Sign in');
    expect(signInButton).toBeInTheDocument();

    const signOutButton = screen.queryByText('Sign out');
    expect(signOutButton).not.toBeInTheDocument();
  });

  it('should call signIn when clicking on the login button', async () => {
    render(<NavBar />);
    fireEvent.click(screen.getByText('Sign in'));
    expect(signIn).toHaveBeenCalledTimes(1);
  });

  it('renders logo and sign-out button when user is authenticated', async () => {
    (useSession as jest.Mock).mockReturnValue({
      data: {
        expires: new Date(Date.now() + 2 * 86400).toISOString(),
        user: {
          id: '1',
          username: 'John',
          roles: ['user'],
          access_token: 'token',
          refresh_token: 'refresh',
        },
      },
      status: 'authenticated',
    });

    render(<NavBar />);

    const logo = screen.getByAltText('logo');
    expect(logo).toBeInTheDocument();

    const signOutButton = screen.getByText('Sign out');
    expect(signOutButton).toBeTruthy();

    const signInButton = screen.queryByText('Sign in');
    expect(signInButton).toBeNull();
  });

  it('should call signOut when clicking on the sign out button', async () => {
    render(<NavBar />);
    fireEvent.click(screen.getByText('Sign out'));
    expect(signOut).toHaveBeenCalledTimes(1);
  });
});
