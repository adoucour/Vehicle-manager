'use client';

import { CustomButtonProps } from '@/types';
import React from 'react';

const CustomButton = ({
  title,
  containerStyles,
  handleClick,
  btnType,
  disabled,
}: CustomButtonProps) => {
  return (
    <button
      type={btnType || 'button'}
      className={`custom-btn ${containerStyles} ${
        disabled ? 'opacity-25' : ''
      }`}
      onClick={handleClick}
      disabled={disabled}
    >
      <span className={`flex-1`}>{title}</span>
    </button>
  );
};

export default CustomButton;
