'use client';
import Link from 'next/link';
import Image from 'next/image';
import { CustomButton } from '.';
import { usePathname } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import { signIn, useSession, signOut } from 'next-auth/react';
import { app_url } from '@/constants';

const NavBar = () => {
  const [hideSign, setHideSign] = useState(false);
  const pathname = usePathname();
  const { data: session } = useSession();

  const routerToLogin = () => {
    signIn();
  };

  const logout = () => {
    signOut();
  };

  useEffect(() => {
    pathname === app_url.login ? setHideSign(false) : setHideSign(true);
  }, [pathname]);

  return (
    <header className="w-full  absolute z-10">
      <nav className="max-w-[1440px] mx-auto flex justify-between items-center sm:px-16 px-6 py-4 bg-transparent">
        <Link href="/" className="flex justify-center items-center">
          <Image
            src="/logo.svg"
            alt="logo"
            width={118}
            height={18}
            className="object-contain"
          />
        </Link>

        {!session?.user && hideSign ? (
          <CustomButton
            title="Sign in"
            btnType="button"
            containerStyles="text-primary-blue rounded-full bg-white min-w-[130px]"
            handleClick={routerToLogin}
          />
        ) : session?.user && hideSign ? (
          <CustomButton
            title="Sign out"
            btnType="button"
            containerStyles="text-primary-blue rounded-full bg-white min-w-[130px]"
            handleClick={logout}
          />
        ) : (
          <></>
        )}
      </nav>
    </header>
  );
};

export default NavBar;
