import React from 'react';
import { render, screen } from '@testing-library/react';
import { Hero } from '.';
import { useSession } from 'next-auth/react';
import '@testing-library/jest-dom';

jest.mock('next-auth/react', () => {
  const originalModule = jest.requireActual('next-auth/react');
  const mockSession = null;
  return {
    __esModule: true,
    ...originalModule,
    useSession: jest.fn(() => {
      return { data: mockSession, status: 'unauthenticated' };
    }),
  };
});

describe('Hero component', () => {
  it('does not display the explore cars button when no user is logged in', () => {
    render(<Hero />);
    expect(screen.queryByRole('button', { name: 'Explore Cars' })).toBeNull();
  });

  it('displays the explore cars button when a user is logged in', () => {
    (useSession as jest.Mock).mockReturnValue({
      data: {
        expires: new Date(Date.now() + 2 * 86400).toISOString(),
        user: {
          id: '1',
          username: 'John',
          roles: ['user'],
          access_token: 'token',
          refresh_token: 'refresh',
        },
      },
      status: 'authenticated',
    });

    render(<Hero />);
    expect(
      screen.getByRole('button', { name: 'Explore Cars' }),
    ).toBeInTheDocument();
  });
});
