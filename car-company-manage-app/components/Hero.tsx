'use client';

import Image from 'next/image';
import { CustomButton } from '.';
import { useSession } from 'next-auth/react';
import React from 'react';

function Hero() {
  const handleScroll = () => {};

  const { data: session } = useSession();

  return (
    <div className="hero">
      <div className="flex-1 pt-36 padding-x">
        <h1 className="hero__title">Rent a car - quickly and easily!</h1>

        <p className="hero__subtitle">
          Streamline your car rental experience with an effortless booking
          process.
        </p>
        {session?.user && (
          <CustomButton
            title="Explore Cars"
            containerStyles=" bg-primary-blue text-white rounded-full mt-10"
            handleClick={handleScroll}
          />
        )}
      </div>
      <div className="hero__image-container">
        <div className="hero__image">
          <Image src="/hero.png" alt="hero" fill className="object-contain" />
        </div>
        <div className="hero__image-overlay" />
      </div>
    </div>
  );
}

export default Hero;
